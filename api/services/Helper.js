module.exports = {

  //Checks if this is the current page according to the corrent controller and
  //action. It is usefull to highlight the active page in the headers navbar
  isCurrentPage: function(req, controller, action) {

    for (var i = 0; i < action.length; ++i){

    if(action[i] == req.options.action){
       return (req.options.controller === controller && req.options.action === action[i])
     }
   }
 },
  /*
  / Get the keys of an object that beginn with a prefix and that are true
  / object: Object
  / prefix: String
  / returns array
  */
  trueKeysWithPrefix: function(object, prefix) {

    var keys = [];

    var prefLength = prefix.length;

    for (var property in object) {
      if (object.hasOwnProperty(property) && property.toString().indexOf(prefix) === 0 && object[property]) {

        //remove prefix and push string to keys[]
        keys.push(property.slice(prefLength));
      }
    }

    return keys;
  }
};
