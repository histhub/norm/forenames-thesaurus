/**
* ForenamePageQueryHelper
*
* @description :: Helper functions for PagesController.forename
* It helps to make PagesController.forename cleaner.
* It makes intese use of ForenamesQueryBuilder to guarantee a generic approach
* when constructing the sql queries across the wohle application.
*/

module.exports = function() {
  var _that = this;

  /**
  * Make sql to get the Conceptual object of the Appellation:
  * - PeIt29646:	Hauptform einer Benennung [deu], or
  *	- PeIt29645:	Variante einer Benennung [deu]
  *
  * @Parameters
  * {String} semkeyAppe Semkey of the appellation (e.g. 'Appe49966' for 'Clainhans')
  *
  * @return: {String} sql
  */
  this.conceptualObject = function(semkeyAppe) {
    return `select
    semkey_target_entity as conceptual_object
    from histhub.entities_association
    where semkey_source_entity = '` + semkeyAppe + `' And fk_entities_association_type = 1`;
  };


  /**
  * Make sql to get BASIC INFORMATIONS about the appellation
  * label, genders, languages, spelling variants
  *
  * @Parameters
  * {String} semkeyAppe Semkey of the appellation (e.g. 'Appe49966' for 'Clainhans')
  *
  * @return: {String} sql
  */
  this.basicInfos = function(semkeyAppe) {

    var query = new ForenamesQueryBuilder();

    query.makeWith();

    query.makeSelect('forenames');

    query.makeFrom();

    query.makeWhere({
      'semkeyAppellation': semkeyAppe,
    });

    query.makeGroupBy('forenames');

    return query.getSql();
  };


  /**
  * Make sql to get NAME HOLDERS of the appellation
  *
  * @Parameters
  * {String} semkeyAppe Semkey of the appellation (e.g. 'Appe49966' for 'Clainhans')
  *
  * @return: {String} sql
  */
  this.nameHolders = function(semkeyAppe) {

    return `
    WITH tw1 AS
    (
      SELECT appe.semkey_appe AS person_appellation_semkey,
             role1.fk_temporal_entity,
             role2.semkey_associated_entity AS hhb_id,
             papp.person_recomposed_appellation AS label,
             ecor_hls.orig_id_in_table hls_id,
             ecor_ssrq.orig_id_in_table ssrq_id
      FROM histhub.appellation_label_component aplc
        LEFT JOIN histhub.appellation appe ON appe.pk_appellation = aplc.fk_appellation
        LEFT JOIN histhub.role role1
               ON role1.semkey_associated_entity = appe.semkey_appe
              AND role1.fk_role_type = 3
        LEFT JOIN histhub.role role2
               ON role2.fk_temporal_entity = role1.fk_temporal_entity
              AND role2.fk_role_type = 4
        LEFT JOIN histhub.v_person_recomposed_appellation papp ON papp.fk_persistent_item = SUBSTR (role2.semkey_associated_entity,5)::INTEGER
        LEFT JOIN histhub.entity_correspondence ecor_hls
               ON ecor_hls.histhub_id_in_table = SUBSTR (role2.semkey_associated_entity,5)
              AND ecor_hls.histhub_table_name = 'histhub.persistent_item'
              AND ecor_hls.orig_table_name = 'histhub_original_data.hls'
        LEFT JOIN histhub.entity_correspondence ecor_ssrq
               ON ecor_ssrq.histhub_id_in_table = SUBSTR (role2.semkey_associated_entity,5)
              AND ecor_ssrq.histhub_table_name = 'histhub.persistent_item'
              AND ecor_ssrq.orig_table_name = 'histhub_original_data.ssrq_names_final'
      WHERE fk_refers_to_appellation = SUBSTR('` + semkeyAppe + `',5)::INTEGER
    )
    SELECT person_appellation_semkey,
           MAX(hhb_id) AS hhb_id,
           MAX(label)  AS label,
           MAX(hls_id) AS hls_id,
           MAX(ssrq_id) AS ssrq_id
    FROM tw1
    GROUP BY person_appellation_semkey
    ORDER BY label asc`;
  };



  /**
  * Make sql to get RELATED NAMES of the appellation.
  * Related Names are Hauptforms of Appellations that have the same
  * 'Appellation as a Concept' as the Appellation in question (with semkeyAppe).
  *
  * @Parameters
  * {String} semkeyAppe Semkey of the appellation (e.g. 'Appe38790' for 'Kleinhans')
  *
  * @return: {String} sql
  */
  this.relatedNames = function(semkeyAppe) {

    var query = new ForenamesQueryBuilder();

    query.makeWith('PeIt29646'); //Only Hauptforms

    query.makeSelect('forenames');

    query.makeFrom({
      'custom': `
      from tw1
      inner join (
        Select
        t5.semkey_associated_entity
        from
        histhub.role t2,
        histhub.role t3,
        histhub.role t4,
        histhub.role t5
        where
        t2.fk_role_type = 3 and t2.semkey_associated_entity = '` + semkeyAppe + `'
        and t3.fk_role_type = 4 and t3.fk_temporal_entity = t2.fk_temporal_entity
        and t4.fk_role_type = 4 and t4.semkey_associated_entity = t3.semkey_associated_entity
        and t5.fk_role_type = 3 and t5.fk_temporal_entity = t4.fk_temporal_entity	and t5.semkey_associated_entity <> '` + semkeyAppe + `'
      ) as similar_appellations on similar_appellations.semkey_associated_entity = tw1.semkey_appellation`
    });

    query.makeGroupBy('forenames');

    return query.getSql();
  };

  /**
  *	Make sql to get HAUPTFORM APPELLATIONS of the Appellation in question
  * (Will only return rows for Variants, e.g. Johanß, not for Hauptforms, e.g. Hans)
  *
  * @Parameters
  * {String} semkeyAppe Semkey of the appellation (e.g. 'Appe38790' for 'Kleinhans')
  *
  * @return: {String} sql
  */
  this.hauptformsOfVariant = function(semkeyAppe) {
    var query = new ForenamesQueryBuilder();

    query.makeWith('PeIt29646');

    query.makeSelect('forenames');

    query.makeFrom({
      'custom': `
      from tw1
      inner join (
        Select
        t1.semkey_target_entity
        from
        histhub.entities_association t1
        where
        t1.semkey_source_entity = '` + semkeyAppe + `' and t1.fk_entities_association_type = 2
      ) as hauptforms_of_variant on hauptforms_of_variant.semkey_target_entity = tw1.semkey_appellation`
    });

    query.makeGroupBy('forenames');

    return query.getSql();
  };

  /**
  *	Make sql to get TEMPORAL DISTRIBUTION
  *
  * @Parameters
  * {String} semkeyAppe Semkey of the appellation (e.g. 'Appe38790' for 'Kleinhans')
  *
  * @return: {String} sql
  */
  this.temporalDistribution = function(semkeyAppe) {
    return `
    WITH RECURSIVE t (ad, af) AS
    (
      VALUES
      (
        0001,
        0050
      )
      UNION ALL
      SELECT ad + 50,
      af + 50
      FROM t
      WHERE (ad + 50) < 2002
    ),
    tw1 AS
    (
      SELECT aplc.fk_refers_to_appellation,
      appe.semkey_appe person_appellation_semkey,
      role1.fk_temporal_entity,
      role2.semkey_associated_entity person_semkey,
      papp.person_recomposed_appellation,
      role3.fk_temporal_entity,
      dati.dating_year birth_year
      FROM histhub.appellation_label_component aplc
      LEFT JOIN histhub.appellation appe ON appe.pk_appellation = aplc.fk_appellation
      LEFT JOIN histhub.role role1
      ON role1.semkey_associated_entity = appe.semkey_appe
      AND role1.fk_role_type = 3
      LEFT JOIN histhub.role role2
      ON role2.fk_temporal_entity = role1.fk_temporal_entity
      AND role2.fk_role_type = 4
      LEFT JOIN histhub.v_person_recomposed_appellation papp ON papp.fk_persistent_item = SUBSTR (role2.semkey_associated_entity,5)::INTEGER
      LEFT JOIN histhub.role role3
      ON role3.semkey_associated_entity = role2.semkey_associated_entity
      AND role3.fk_role_type = 5
      LEFT JOIN histhub.dating dati
      ON dati.fk_temporal_entity = role3.fk_temporal_entity
      AND dati.fk_system_type = 19
      WHERE fk_refers_to_appellation = SUBSTR('` + semkeyAppe + `',5)::Integer
      ORDER BY birth_year
    )
    SELECT
    t.ad as start,
    t.af as end,
    concat(t.ad || '-' || t.af) as period,
    COUNT(tw1.birth_year) as count
    FROM t
    LEFT JOIN tw1
    ON tw1.birth_year BETWEEN t.ad AND t.af
    GROUP BY t.ad, t.af
    --HAVING STRING_AGG(tw1.birth_year::text,';') IS NOT NULL
    ORDER BY t.ad`;
  };

};
