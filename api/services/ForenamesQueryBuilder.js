/**
* ForenamesQueryBuilder
*
* @description :: Server-side logic for building queries in sql to find forenames.
* Attention: It is about finding lists of multiple forenames and helper queries
* e.g. to find the languages of a certain subset of forenames.
* Those helper queries are used to empower the GUI of the search page.
*/

module.exports = function() {
  var _that = this;

  /**
  * Make sql with clause
  * @Parameters
  * {String} appellationType optional persisten item id of the forename concept
  *      Haputform (PeIt29646) or Variant (PeIt29645).
  *      Values: ['PeIt29646', 'PeIt29645'], Default: ''
  *      (This where condition is in WITH instead of SEKECT for Performance reasons)
  * @return: {String} select clause
  */
  this.makeWith = function(appellationType) {

    var replaceString = '';

    if(appellationType === "PeIt29646" || appellationType === "PeIt29645"){
      replaceString = "WHERE appellation_type = '" + appellationType + "'";
    }

    _that.with = `
    WITH tw1 AS
    (
      SELECT
      frequence,
      pk_persistent_item,
      semkey_peit,
      begin_year,
      end_year,
      semkey_appellation,
      labels_appellation,
      semkey_appellation_variant,
      appellation_variant_label,
      appellation_gender,
      semkey_language,
      labels_language
      FROM tbrowser.vm_forename_teen_appellation_duration_gender_language_variant
      ` + replaceString + `
    )
    `;

    return _that.with;
  };


  /**
  * Make sql select clause
  * @Parameters
  * {String} queryType e.g. forenames, languageCounts]
  *
  * @return: {String} select clause
  */
  this.makeSelect = function(queryType) {

    var forenamesSelect = `
    SELECT

    semkey_appellation as id,
    labels_appellation as label,

    --aggregate Appellation Variants of the Appellation Hauptform in json object (array would be better, but much more difficult to implement. so its ok like that)
    COALESCE(json_object_agg(distinct semkey_appellation_variant, appellation_variant_label ORDER BY appellation_variant_label) FILTER (WHERE semkey_appellation_variant IS NOT NULL), '{}')::json as appellation_variants,

    --aggregate Genders of the Appellations Hauptform in json object
    json_agg(DISTINCT appellation_gender) as genders,

    --aggregate Languages of the Appellation Hauptform in json object
    COALESCE(json_object_agg(distinct semkey_language, labels_language) FILTER (WHERE semkey_language IS NOT NULL), '{}')::json as languages,

    min(begin_year) as earliest_year,
    max(end_year) as latest_year,
    coalesce(frequence, 0) as name_holders_count,
    count(*) OVER() AS full_count
    `;

    var languageCountsSelect = `
    SELECT distinct semkey_language as semkey, labels_language as label, count(distinct semkey_appellation)
    `;

    //TODO replace the appellation_gender with the persistent item id (the label could change one day)
    var genderCountsSelect = `
    SELECT distinct appellation_gender as label, count(distinct semkey_appellation)
    `;

    var allLanguagesSelect = `
    SELECT distinct
    semkey_language as semkey,
    labels_language as label
    `;

    var earliestAndLatestYearSelect = `
    SELECT min(begin_year), max(end_year)
    `;

    switch (queryType) {

      case 'forenames':
      _that.select = forenamesSelect;
      break;

      case 'languageCounts':
      _that.select = languageCountsSelect;
      break;

      case 'genderCounts':
      _that.select = genderCountsSelect;
      break;

      case 'allLanguages':
      _that.select = allLanguagesSelect;
      break;

      case 'earliestAndLatestYear':
      _that.select = earliestAndLatestYearSelect;
      break;

    }



    return _that.select;
  };

  /**
  * Make sql from clause
  * @return: {String} from clause
  */
  this.makeFrom = function(options) {
    _that.from = `from tw1
    `;

    if(options){
      if(options.custom){
        _that.from = options.custom;
      }
    }
    return _that.from;
  };

  /**
  * Make sql where clause
  * @return: {String} where clause
  */
  this.makeWhere = function(options) {
    //array of where conditions that will be joined with AND
    var andWheres = [];

    if (options.semkeyAppellation) {
      var semkeyWhere = "semkey_appellation = '" + options.semkeyAppellation + "'"
      andWheres.push(semkeyWhere);
    }

    //adds search string and matchType to where clause
    if (options.searchString) {

      switch (options.matchType) {
        case 'contains':
        var prefix = '%';
        var suffix = '%';
        break;

        case 'startsWith':
        var prefix = '';
        var suffix = '%';
        break;

        case 'endsWith':
        var prefix = '%';
        var suffix = '';
        break;

        case 'equals':
        var prefix = '';
        var suffix = '';
        break;

        default:
        var prefix = '%'; //default
        var suffix = '%'; //default
      }

      var searchString = prefix + options.searchString + suffix;

      //if searchin only in spelling variants


      switch (options.searchIn) {
        case 'historical':
        var searchStringWhere = `
        appellation_variant_label ilike '` + searchString + `'
        `;
        break;

        case 'all':
        var searchStringWhere = `
        (
          labels_appellation ilike '` + searchString + `'
          or
          appellation_variant_label ilike '` + searchString + `'
        )`;
        break;

        default:
        var searchStringWhere = `
        (
          labels_appellation ilike '` + searchString + `'
          or
          appellation_variant_label ilike '` + searchString + `'
        )`;
      }

      andWheres.push(searchStringWhere);
    }

    if (options.languagesArray) {
      if (options.languagesArray.length > 0) {
        var languageWhere = "labels_language IN ('" + options.languagesArray.join("','") + "')"
        andWheres.push(languageWhere);
      }
    }

    if (options.gendersArray) {
      if (options.gendersArray.length > 0) {
        var genderWhere = "appellation_gender IN ('" + options.gendersArray.join("','") + "')"
        andWheres.push(genderWhere);
      }
    }

    if (options.startYear && options.endYear) {
      var timeWhere = `
      begin_year <= ` + options.endYear + `::integer and end_year >= ` + options.startYear + `
      `;

      andWheres.push(timeWhere);
    }

    _that.where = '';

    if(andWheres.length > 0){
      _that.where = 'WHERE ' + andWheres.join(`
        AND
        `)
      }

      return _that.where;
    };

    /**
    * Make sql groupBy clause
    * @return: {String} groupBy clause
    */
    this.makeGroupBy = function(queryType) {

      var forenamesGroupBy = `
      group by
      pk_persistent_item,
      semkey_peit,
      semkey_appellation,
      labels_appellation,
      frequence
      `;

      var languageCountsGroupBy = `
      group by semkey_language, labels_language
      `;

      var genderCountsGroupBy = `
      group by appellation_gender
      `;

      switch (queryType) {

        case 'forenames':
        _that.groupBy = forenamesGroupBy;
        break;

        case 'languageCounts':
        _that.groupBy = languageCountsGroupBy;
        break;

        case 'genderCounts':
        _that.groupBy = genderCountsGroupBy;
        break;
      }

      return _that.groupBy;
    };

    /**
    * Make sql orderBy clause
    * @return: {String} orderBy clause
    */
    this.makeOrderBy = function(options) {

      //Default
      var order = 'label DESC';

      if (options.order) {
        order = options.order;
      }

      _that.orderBy = `order by ` + order;

      return _that.orderBy;
    };

    /**
    * Make sql limitAndOffset clause
    * @return: {String} limitAndOffset clause
    */
    this.makeLimitAndOffset = function(options) {

      //Default
      var perPage = 20;

      // if(options.perPage) {
      // 	perPage = options.perPage;
      // 	if(perPage>1000){
      // 		perPage=1000;
      // 	}
      // }

      //Default
      var offset = 0;

      if (options.currentPage) {
        offset = (options.currentPage - 1) * perPage;
      }

      _that.limitAndOffset = 'LIMIT ' + perPage + ' OFFSET ' + offset;

      return _that.limitAndOffset;
    };

    this.getSql = function() {
      var sql = [this.with, this.select, this.from, this.where, this.groupBy, this.orderBy, this.limitAndOffset].join(`
        `);
        // console.log(sql);
        return sql;
      };
    };
