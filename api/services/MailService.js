/**
* Mail
*
* @description :: Server-side logic for building forenames queries in sql.
*/

module.exports = function(){

  /**
  * get transporter
  */
  this.getTransporter = function(){
    var nodemailer = require('nodemailer');
		var sendmailTransport = require('nodemailer-sendmail-transport');

		// create reusable transporter object using the default SMTP transport
		return nodemailer.createTransport(sendmailTransport({
			path: '/usr/lib/sendmail'
		}));
  }
}
