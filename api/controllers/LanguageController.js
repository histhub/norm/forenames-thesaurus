/**
* LanguageController
*
* @description :: Server-side logic for redirecting to the language slug
* according to Accept-Language Header of the request
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {
	redirectLanguage: function (req, res) {

		switch (req.locale) {
			case 'de':
			res.redirect('/forenames/de');
			break;

			case 'it':
			res.redirect('/forenames/it');
			break;

			case 'fr':
			res.redirect('/forenames/fr');
			break;

			case 'en':
			res.redirect('/forenames/en');
			break;

			default:
			res.redirect('/forenames/en');

		}
	}
};
