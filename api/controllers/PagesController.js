/**
* PagesController
*
* @description :: Server-side logic for managing Pages
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {
	home: function (req, res) {
		var lang = req.params.lang;

		//make sql to get earliest and latest year
		var query = new ForenamesQueryBuilder();

		query.makeWith();

		query.makeFrom();

		query.makeSelect('earliestAndLatestYear');

		var sql = query.getSql();

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){
				var earliestYear = rows[0].min;
				var latestYear = rows[0].max;
				res.view('home', {layout: 'layout', lang: lang, earliestYear, latestYear});

			},function(error){

				return res.serverError(error);

			}
		)
	},
	contact: function (req, res) {
		res.view('contact', {layout: 'layout', title: 'Contact'});
	},
	login: function (req, res) {
		res.view('engage1', {layout: 'layout',  title: 'Login'});
	},
	engage1: function (req, res) {
		res.view('engage1', {layout: 'layout'});
	},
	engage2: function (req, res) {
		res.view('engage2', {layout: 'layout', title: 'feedback'});
	},
	forename: function (req, res) {

		//get the semkeyAppe of the forename
		var semkeyAppe = req.param('id')

		//make a new queryHelper object (defined in api/services)
		var queryHelper = new ForenamePageQueryHelper();

		/*
		* Make sql to get the Conceptual object of the Appellation
		*/
		var sqlConceptualObject = queryHelper.conceptualObject(semkeyAppe);


		/*
		* Get the Conceptual object of the Appellation. This may be:
		* 'PeIt29646':	Hauptform einer Benennung [deu]
		*	'PeIt29645':	Variante einer Benennung [deu]
		*/
		sequelize.query(sqlConceptualObject, {type: sequelize.QueryTypes.SELECT} ).then(
			function(result){


				/*
				* Handle incoming requests for Hauptforms
				*/
				if(result[0].conceptual_object == 'PeIt29646'){


					Promise.all([
						/*
						* Get sql to get basic informations
						*/
						sequelize.query(queryHelper.basicInfos(semkeyAppe)),

						/*
						*  Get sql to get Name Holders
						*/
						sequelize.query(queryHelper.nameHolders(semkeyAppe)),

						/*
						*  Get sql to get related names
						*/
						sequelize.query(queryHelper.relatedNames(semkeyAppe)),

						/*
						* Make sql to get temporal distribution
						*/
						sequelize.query(queryHelper.temporalDistribution(semkeyAppe)),

					]).then(
						function(resultsArray){

							var appellation = resultsArray[0][0][0]; //from basicInfos
							//get the displayLabels for all langages in the array
							appellation.languages = Iso639_2.getLabels(appellation.languages, req.params['lang']);

							var persons_with_appe = resultsArray[1][0]; //from nameHolders


							var related_names = resultsArray[2][0]; //from relatedNames
							for (var i = 0; i < related_names.length; i++) {
								related_names[i].languages =  Iso639_2.getLabels(related_names[i].languages, req.params['lang']);
							}

							//sort related_names alphabetically. Fix for the case of, e.g., Hans, where after Zane came Grosshans. In "after-Zane" group were those concepts, where earliest and latest years were null.
							function compare(a,b) {
								if (a.label < b.label)
									return -1;
								if (a.label > b.label)
									return 1;
								return 0;
							}
							related_names.sort(compare);
							//check if any temporal information;
							var timePeriods = resultsArray[3][0];
							var hasTimeInfo = false;
							var appellation_timeline = false;
							for (period of timePeriods) {
								if(parseInt(period.count) > 0){
									hasTimeInfo = true;
								}
							}
							//if has time info, pass the results to the page
							if(hasTimeInfo){
								var appellation_timeline = JSON.stringify(resultsArray[3][0]); //from temporalDistribution
							}else{
								appellation_timeline = false;
							}

							return res.view('forename_hauptform', {
								layout: 'layout',
								title: 'FirstName',
								id: semkeyAppe,
								appellation: appellation,
								timeline: appellation_timeline,
								persons_with_appe: persons_with_appe,
								related_names: related_names
							});
						},
						function(error){
							return res.serverError(error);
						}
					);
				}


				/*
				* Handle incoming requests for Variants
				*/
				else if(result[0].conceptual_object == 'PeIt29645'){


					Promise.all([
						/*
						* Get sql to get basic informations
						*/
						sequelize.query(queryHelper.basicInfos(semkeyAppe)),

						/*
						*  Get sql to get Name Holders
						*/
						sequelize.query(queryHelper.nameHolders(semkeyAppe)),

						/*
						* Make sql to get temporal distribution
						*/
						sequelize.query(queryHelper.temporalDistribution(semkeyAppe)),

						/*
						*	Get sql to get Hauptform Appellations
						*/
						sequelize.query(queryHelper.hauptformsOfVariant(semkeyAppe))

					]).then(
						function(resultsArray){

							var appellation = resultsArray[0][0][0]; //from basicInfos
							//get the displayLabels for all langages in the array
							appellation.languages = Iso639_2.getLabels(appellation.languages, req.params['lang']);

							var persons_with_appe = resultsArray[1][0]; //from nameHolders

							//check if any temporal information;
							var timePeriods = resultsArray[2][0];
							var hasTimeInfo = false;
							var appellation_timeline = false;
							for (period of timePeriods) {
								if(parseInt(period.count) > 0){
									hasTimeInfo = true;
								}
							}
							//if has time info, pass the results to the page
							if(hasTimeInfo){
								var appellation_timeline = JSON.stringify(resultsArray[2][0]); //from temporalDistribution
							}else{
								appellation_timeline = false;
							}

							var hauptforms_of_variant = resultsArray[3][0]; //from hauptformsOfVariant
							for (var i = 0; i < hauptforms_of_variant.length; i++) {
								hauptforms_of_variant[i].languages =  Iso639_2.getLabels(hauptforms_of_variant[i].languages, req.params['lang']);
							}

							return res.view('forename_variant', {
								layout: 'layout',
								title: 'FirstName',
								id: semkeyAppe,
								appellation: appellation,
								timeline: appellation_timeline,
								persons_with_appe: persons_with_appe,
								hauptforms_of_variant: hauptforms_of_variant
							});
						},
						function(error){
							return res.serverError(error);
						}
					);
				}

				/*
				* Handle error, if Appellation is no Variant and no Hauptform
				*/
				else{
					return res.serverError('Oups! Something went wrong! :-(');
				}

			},function(error){

				return res.serverError(error);

			}
		)

	},
	project: function (req, res) {
		lang = req.params['lang'] ;
		if (lang == 'it'){
			res.view('project_it', {layout: 'layout', title: 'Project'});
		}
		else if (lang == 'en'){
			res.view('project_en', {layout: 'layout', title: 'Project'});
		}
		else if (lang == 'fr'){
			res.view('project_fr', {layout: 'layout', title: 'Project'});
		}
		else{
		res.view('project', {layout: 'layout', title: 'Project'});}
	},
	search: function (req, res) {
		res.view('search', {layout: 'layout', title: 'Search'});
	},
	hhb_resource: function (req, res) {
		res.view('hhb_resource', {layout: 'layout'});
	},
	sitemap: function (req, res) {

		var query = 'SELECT semkey_appellation, labels_appellation, semkey_appellation_variant, appellation_variant_label FROM tbrowser.vm_forename_teen_appellation_duration_gender_language_variant';

		sequelize.query(query, {type: sequelize.QueryTypes.SELECT} ).then(
		function(result){
			var unique_appes = [];
			for ( var i=0; i< result.length; i++)
			{
					var app_pair = [];
					app_pair.push(result[i]['semkey_appellation']); //AppeXXX
					app_pair.push(result[i]['labels_appellation']); //label
					unique_appes.push(app_pair);
				if (result[i]['semkey_appellation_variant']){
					var var_app_pair = [];
					var_app_pair.push(result[i]['semkey_appellation_variant']); //AppeXXX
					var_app_pair.push(result[i]['appellation_variant_label']); //label

						unique_appes.push(var_app_pair);
			}
			}
			//remove duplicated pairs Appe-label
				var hash = {};
				var out = [];
				for (var i = 0, l = unique_appes.length; i < l; i++) {
				  var key = unique_appes[i].join('|');
				  if (!hash[key]) {
				    out.push(unique_appes[i]);
				    hash[key] = 'found';
				  }
				}

									return res.view('sitemap', {
										layout: 'layout',
										title: 'Sitemap',
										result:out
									});
								},
								function(error){
									return res.serverError(error);
								}
							);
	},
	systemtypes: function (req, res) {
		SystemType.findAll().then(
			function (systemTypes) {
				return res.view('systemtypes', {layout: 'layout', types: systemTypes});
			},
			function (err){
				if (err) {
					return res.serverError(err);
				}
			}
		);
	}

};
