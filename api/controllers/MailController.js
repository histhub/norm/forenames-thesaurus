/**
* MailController
*
* @description :: Server-side logic for sending Mails
*/

module.exports = {

	/*
	* Called by
	* /forenames/de/api/register-new-mail
	*/
	newMail: function (req, res) {

		var transporter = new MailService().getTransporter();

		if(req.body.email){
			var emailAddress = req.body.email;
		}

		// setup e-mail data with unicode symbols
		var mailOptions = {
			from: '"histHub Thesaurus Browser" <forenames@ssrq-sds-fds.ch>', // sender address
			to: 'pascale.sutter@ssrq-sds-fds.ch', // list of receivers
			cc: 'jonas.schneider@dhs.ch', // list of receivers
			subject: 'Theasaurus Browser: New Email Address', // Subject line
			text: 'New Email Address registered: ' + emailAddress // plaintext body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
			if (error) {
				res.json(error);
			}
			res.json({
				'registeredEmailAddress': emailAddress,
				'info': info
			});
		});

	},
	/*
	* Called by
	* /forenames/de/api/send-feedback
	*/
	feedback: function (req, res) {

		var transporter = new MailService().getTransporter();

		// setup e-mail data with unicode symbols
		var mailOptions = {
			from: '"histHub Thesaurus Browser" <forenames@ssrq-sds-fds.ch>', // sender address
			to: 'pascale.sutter@ssrq-sds-fds.ch', // list of receivers
			cc: 'jonas.schneider@dhs.ch', // list of receivers
			subject: 'Theasaurus Browser: New Feedback', // Subject line
			text:  'E-Mail: ' +	req.body.email + '\n' +
			'First Name: ' +	(req.body.firstname ? req.body.firstname : '[not entered by user]') + '\n' +
			'Last Name: ' +	(req.body.lastname ? req.body.lastname : '[not entered by user]') + '\n' +
			'Feedback Message: \n \n' +
			req.body.message
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
			if (error) {
				res.json(error);
			}
			res.json({
				'info': info
			});
		});

	}




};
