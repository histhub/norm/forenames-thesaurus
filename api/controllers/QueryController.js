/**
* QueryController
*
* @description :: Server-side logic for searching forenames.
*									It is the core-element of the Forenames Query API.
*									It transforms incoming Request Parameters in DB-Queries
*									and performes the response.
* @help		   :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {
	forenames: function (req, res) {

		//new fornename query builder object (api/services/ForenamesQuery.js)
		var query = new ForenamesQueryBuilder();

		query.makeWith('PeIt29646');

		query.makeSelect('forenames');

		query.makeFrom();

		query.makeWhere({
			'searchString' : req.body.searchString,
			'matchType': req.body.matchType,
			'searchIn': req.body.searchIn,
			'startYear': req.body.startYear,
			'endYear': req.body.endYear,
			'languagesArray': Helper.trueKeysWithPrefix(req.body, 'lang_'),
			'gendersArray': Helper.trueKeysWithPrefix(req.body, 'gender_')
		});

		query.makeGroupBy('forenames');

		if(req.body.order){
			//replace underscore: 'lang_DESC' -> 'lang DESC'
			var order = req.body.order.replace('_DESC', ' DESC').replace('_ASC', ' ASC');
		}

		query.makeOrderBy({
			'order': order
		});

		query.makeLimitAndOffset({
			'currentPage': req.body['currentPage']
		});

		var sql = query.getSql();

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(result){

				var rows = [];
				var count = 0;

				var data = {
					'rows' : rows,
					'count': count
				}

				for (var row in result) {
					if (result.hasOwnProperty(row)) {
						//ad full_count to count parameter
						data.count = result[row].full_count
						//delete full_count in this row
						delete result[row].full_count;

						//prepare languages property
						var langs = [];
						for (key in result[row].languages) {
							langs.push({
								'semkey':key,
								'iso_639_2': result[row].languages[key],
								'displayLabel': Iso639_2.getLabel(result[row].languages[key], req.params['lang'])
							})
						}
						result[row].languages = langs;

						data.rows.push(result[row]);
					}
				}

				return res.json(200, data);

			},function(error){

				return res.serverError(error);

			}
		)


	},
	languageCounts: function(req, res){

		//new fornename query builder object (api/services/ForenamesQuery.js)
		var query = new ForenamesQueryBuilder();

		query.makeWith('PeIt29646');

		query.makeSelect('languageCounts');

		query.makeFrom();

		query.makeWhere({
			'searchString' : req.body.searchString,
			'matchType': req.body.matchType,
			'startYear': req.body.startYear,
			'endYear': req.body.endYear,
			'searchIn': req.body.searchIn,
			'gendersArray': Helper.trueKeysWithPrefix(req.body, 'gender_')
		});

		query.makeGroupBy('languageCounts');

		query.makeOrderBy({
			'order': 'labels_language asc'
		});

		var sql = query.getSql();

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){

				return res.json(200, rows);

			},function(error){

				return res.serverError(error);

			}
		)
	},
	allLanguages: function(req, res){

		var query = new ForenamesQueryBuilder();

		query.makeWith('PeIt29646');

		query.makeFrom();

		query.makeSelect('allLanguages');

		var sql = query.getSql();

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){

				for (var row in rows) {
					if (rows.hasOwnProperty(row)) {
						rows[row].displayLabel = Iso639_2.getLabel(rows[row].label, req.params['lang']);
						if(rows[row].displayLabel === rows[row].label){
							rows[row].labelNotFound = true;
						}
					}
				}

				return res.json(200, rows);

			},function(error){

				return res.serverError(error);

			}
		)
	},
	earliestAndLatestYear: function(req, res){

		var query = new ForenamesQueryBuilder();

		query.makeWith('PeIt29646');

		query.makeFrom();

		query.makeSelect('earliestAndLatestYear');

		var sql = query.getSql();

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){

				return res.json(200, rows);

			},function(error){

				return res.serverError(error);

			}
		)
	},
	genderCounts: function(req, res){

		//new fornename query builder object (api/services/ForenamesQuery.js)
		var query = new ForenamesQueryBuilder();

		query.makeWith('PeIt29646');

		query.makeSelect('genderCounts');

		query.makeFrom();

		query.makeWhere({
			'searchString' : req.body.searchString,
			'matchType': req.body.matchType,
			'startYear': req.body.startYear,
			'endYear': req.body.endYear,
			'searchIn': req.body.searchIn,
			'languagesArray': Helper.trueKeysWithPrefix(req.body, 'lang_'),
		});

		query.makeGroupBy('genderCounts');

		var sql = query.getSql();

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){

				return res.json(200, rows);

			},function(error){

				return res.serverError(error);

			}
		)
	},
	appellationSearch: function(req, res){

		//returns forename appellations from appellation table matching a prefix

		var sql = `
      SELECT pk_appellation pk,
             provisional_label forename,
             provisional_language lang,
             provisional_gender_use gender
      FROM histhub.appellation
      WHERE LOWER(provisional_label) LIKE '` + req.query.query + `%'
      AND fk_system_type=7
      ORDER BY provisional_label
		`;

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){

				return res.json(200, rows);

			},function(error){

				return res.serverError(error);

			}
		)
	},
	labelSearch: function(req, res){

		//returns forename appellations from appellation table matching a prefix

		var sql = `
      SELECT pk_appellation AS id,
             provisional_label || ' (' || provisional_gender_use || '/' || provisional_language || ')' AS label
      FROM histhub.appellation
      WHERE LOWER(provisional_label) LIKE LOWER('` + req.query.term + `%')
      AND fk_system_type = 7
      AND strpos(provisional_label, ' ') = 0
      AND strpos(provisional_label, '-') = 0
      ORDER BY provisional_label
    `;

		sequelize.query(sql, {type: sequelize.QueryTypes.SELECT} ).then(
			function(rows){

				return res.json(200, rows);

			},function(error){

				return res.serverError(error);

			}
		)
	},
};
