/**
 * SystemType.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    pk_system_type: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    label: {
      type: Sequelize.STRING
    },
    definition: {
      type: Sequelize.TEXT
    },
    notes: {
      type: Sequelize.TEXT
      }
  },
  options: {
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
    schema: 'histhub',
    tableName: 'system_type'
  },
  associations: function(){
    SystemType.hasMany(Appellation,{as: 'Appellations', foreignKey:'fk_system_type'});
  }
};
