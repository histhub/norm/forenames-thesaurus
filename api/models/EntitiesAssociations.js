/**
 * EntitiesAssociations.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  options: {
    timestamps: false,
    schema: 'histhub',
    tableName: 'entities_associations'
  },
  attributes: {
    id: {
      field: 'pk_entities_association',
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    semkey_source_entity: {
      type: Sequelize.STRING,
    },
    semkey_target_entity: {
      type: Sequelize.STRING,
    },
    fk_entities_association_type: {
      type: Sequelize.STRING
    }
  }
};
