/**
 * AppellationLabelComponent.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  options: {
    timestamps: false,
    schema: 'histhub',
    tableName: 'appellation_label_component'
  },
  associations: function(){
    AppellationLabelComponent.belongsTo(Appellation, {foreignKey: 'fk_appellation', targetKey: 'id'});
  },
  attributes: {
    id: {
      field: 'pk_appellation_label_component',
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    fk_appellation: {
      type: Sequelize.INTEGER,
    },
    fk_system_type: {
      type: Sequelize.INTEGER,
    },
    label: {
      type: Sequelize.STRING
    }
  }
};
