/**
 * Appellation.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 module.exports = {
   options: {
     timestamps: false,
     schema: 'histhub',
     tableName: 'appellation',
     scopes: {
       forenameAppellation: {
         where: {
          fk_system_type: 7
        }
       }
     }
   },
   associations: function(){
     Appellation.belongsTo(SystemType, {foreignKey: 'fk_system_type', targetKey: 'pk_system_type'});
     Appellation.hasMany(AppellationLabelComponent, {foreignKey: 'fk_appellation'});
   },
   attributes: {
     id: {
       field: 'pk_appellation',
       type: Sequelize.INTEGER,
       primaryKey: true,
       autoIncrement: true
     },
     label: {
       field: 'provisional_label',
       type: Sequelize.STRING
     },
     gender: {
       field: 'provisional_gender_use',
       type: Sequelize.STRING
     },
     language:{
       field: 'provisional_language',
       type: Sequelize.STRING
     }
   }
  };
