/**
* localize
*
* @module      :: Policy
* @description :: Simple policy to change the request locale according to the
*                 :lang slug in the url in order to show the right GUI language
*/
module.exports = function(req, res, next) {

  req.setLocale(req.param('lang'));

  next();

};
