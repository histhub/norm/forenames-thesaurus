/**
 * `deploy`
 *
 * ---------------------------------------------------------------
 *
 * Grunt tasklist for deployment.
 *
 * Deployment involves the following steps:
 * 1. stopping Thesaurus Browser on test server (using pm2)
 * 2. rsyncing code on development machine and test server (excluding files in .gitignore)
 * 3. running 'npm install' on test server
 * 4. starting Thesaurus Browser on test server (using pm2)
 *
 */

module.exports = function(grunt) {

 var remoteHostConfig = {
    host: 'histhub.ssrq-sds-fds.ch',
    port: 1923,
    user: 'node'
  };

  var FilesToSync = '.sailsrc app.js package.json Gruntfile.js README.md api assets config tasks views';

  var CopyToWWW = 'favicon.ico images min styles';

  grunt.initConfig({
    exec: {
      ssh: {
        cmd: function(remoteCmd) {
          return 'ssh -p ' + remoteHostConfig.port + ' -l ' + remoteHostConfig.user + ' ' +
                 remoteHostConfig.host + ' ' + remoteCmd;
        }
      },
      ssh_sloppy: {
        cmd: function(remoteCmd) {
          return 'ssh -p ' + remoteHostConfig.port + ' -l ' + remoteHostConfig.user + ' ' +
                 remoteHostConfig.host + ' ' + remoteCmd;
        },
        exitCode: [0, 1]
      },
      rsync: {
        cmd: function(sources, target) {
          return "rsync -Cav --rsh='ssh -p " + remoteHostConfig.port + "' --delete-excluded" +
                 " --exclude '.DS_Store' --exclude '.gitkeep' " + sources + " " + remoteHostConfig.user +
                 "@" + remoteHostConfig.host + ":ThesaurusBrowser/" + target;
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('showTB-test', [
    'exec:ssh:pm2 show tbrowser-test'
  ]);

  grunt.registerTask('test', [
    'exec:ssh_sloppy:"pm2 id tbrowser-test && pm2 delete tbrowser-test"',
    'exec:rsync:' + FilesToSync + ':test',
    'exec:ssh:"cd ThesaurusBrowser/test; npm install"',
    'exec:ssh:pm2 start ThesaurusBrowser/test/app.js --name tbrowser-test',
    'exec:ssh:sleep 30',
    'exec:ssh:"cd /var/www/forenames-test; rm -rf ' + CopyToWWW + '"',
    'exec:ssh:"cd ThesaurusBrowser/test/.tmp/public; cp -pr ' + CopyToWWW + ' /var/www/forenames-test"'
  ])

  grunt.registerTask('showTB-prod', [
    'exec:ssh:pm2 show tbrowser-prod'
  ]);

  grunt.registerTask('prod', [
    'exec:ssh_sloppy:"pm2 id tbrowser-prod && pm2 delete tbrowser-prod"',
    'exec:rsync:' + FilesToSync + ':prod',
    'exec:ssh:"cd ThesaurusBrowser/prod; npm install"',
    'exec:ssh:PORT=4711 pm2 start ThesaurusBrowser/prod/app.js --name tbrowser-prod',
    'exec:ssh:sleep 30',
    'exec:ssh:"cd /var/www/forenames-prod; rm -rf ' + CopyToWWW + '"',
    'exec:ssh:"cd ThesaurusBrowser/prod/.tmp/public; cp -pr ' + CopyToWWW + ' /var/www/forenames-prod"'
  ])

  grunt.registerTask('default', [
    'test'
  ]);
};
