/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  '/forenames/': 'LanguageController.redirectLanguage',
  '/forenames/:lang': {
    controller: 'PagesController',
    action: 'home',
    skipAssets: true //details see here http://sailsjs.org/documentation/concepts/routes/url-slugs
  },
  '/forenames/:lang/home': 'PagesController.home',
  '/forenames/:lang/contact': 'PagesController.contact',
  '/forenames/:lang/login': 'PagesController.login',
  '/forenames/:lang/search': 'PagesController.search',
  '/forenames/:lang/forename/:id' : 'PagesController.forename',
  '/forenames/:lang/project' : 'PagesController.project',
  '/forenames/:lang/teaser' : 'PagesController.engage1',
  '/forenames/:lang/feedback' : 'PagesController.engage2',
  '/forenames/:lang/sitemap' : 'PagesController.sitemap',
  '/forenames/:lang/api/forenames' : 'Query.forenames',
  '/forenames/:lang/api/all-languages' : 'Query.allLanguages',
  '/forenames/:lang/api/language-counts' : 'Query.languageCounts',
  '/forenames/:lang/api/gender-counts' : 'Query.genderCounts',
  '/forenames/:lang/api/earliest-and-latest-year' : 'Query.earliestAndLatestYear',
  '/forenames/:lang/api/register-new-mail' : 'MailController.newMail',
  '/forenames/:lang/api/send-feedback' : 'MailController.feedback',
  '/forenames/:lang/api/appellation-search' : 'Query.appellationSearch',
  '/forenames/:lang/api/label-search' : 'Query.labelSearch',
  '/forenames/:lang/hhb_resource' : 'PagesController.hhb_resource',




  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
