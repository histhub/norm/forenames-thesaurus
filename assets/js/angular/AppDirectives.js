angular.module('App')
.directive('languageChanger', ['$location', function($location) {
  return {
    restrict: 'A',
    scope : {
      languageChanger : '@',
      href: '@'
    },
    link : function(scope, element, attrs) {

      scope.baseHref = attrs.href;

      scope.$on('$locationChangeSuccess', function(){
        var newHref = scope.baseHref;
        if($location.url().length > 0){
          newHref = scope.baseHref + '#' + $location.url();
        }
        element.attr('href', newHref);
      })
    }
  };
}])
.directive('updateOnEnter', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      // scope.updateModel = function(){
      //   ctrl.$commitViewValue();
      //   scope.$apply(ctrl.$setTouched);
      // }
      element.on("keyup", function(ev) {
        if (ev.keyCode == 13) {
          scope.queryNow();
        }
      });
    }
  }
})
.directive('focus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    scope : {
      trigger : '@focus'
    },
    link : function(scope, element) {
      scope.$watch('trigger', function(value) {
        if (value === "true") {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
    }
  };
}])
.directive('nameHoldersCount', function() {
  return {
    restrict: 'E',
    scope: {
      'count': '=',
      'nameId': '='
    },
    templateUrl:  'nameHoldersCount.html'
  }
})
.directive('searchFilterPanel', function() {
  return {
    restrict: 'E',
    templateUrl:  'searchFilterPanel.html'
  }
})
.directive('searchResultsPanel', function() {
  return {
    restrict: 'E',
    templateUrl:  'searchResultsPanel.html'
  }
})
.directive('dropdown', function() {
  return {
    restrict: 'E',
    require: '^ngModel',
    scope: {
      options: '=',   // array with objects to select from
      keyToBind: '@', // key in objects to bind (in keyBinding)
      keyBinding: '=', // binds the key to a models
      default: '@', // default key
      ngModel: '=', // selection
      btnClass: '@' // css class for button styling
    },
    link: function(scope, element, attrs) {
      element.on('click', function(event) {
        event.preventDefault();
      });

      scope.isButton = 'isButton' in attrs;

      var updateSelectedOption = function(key){
        angular.forEach(scope.options, function(option){
          if(option[scope.keyToBind] === key){
            scope.ngModel = option;
          }
        })
      }

      scope.$watch('keyBinding', function(newVal,oldVal){
        updateSelectedOption(newVal);
      })

      // selection changed handler
      scope.select = function(option) {
        scope.ngModel = option;

        if (scope.keyToBind) {
          scope.keyBinding = option[scope.keyToBind];
        }
      };

      //Initialize Default selection
      if(scope.default){

        //if no key provided
        if(!scope.keyBinding){

          updateSelectedOption(scope.default);

          if (scope.keyToBind) {
            scope.keyBinding = scope.default;
          }
        }

      }else{
        scope.default = 'Please select item';
      }

    },
    templateUrl: 'dropdown.html'
  };
})
.directive('languageFilter', function() {
  return {
    restrict: 'E',
    scope: {
      filterCounts: '=',
      queryObject: '='
    },
    controller: ['$scope', '$http', function($scope, $http){

      $scope.o = {'selected': []};


      /*
      * fire http request to the languages search api
      * get all languages currently in use with iso key and translated language name
      */
      $http({
        method: 'POST',
        url: 'api/all-languages'
      }).then(function successCallback(response) {

        //retrieve results
        $scope.options = response.data;

        //preselect languages
        angular.forEach($scope.options, function(option){
          if($scope.queryObject['lang_'+ option.label]){

            $scope.o.selected.push(option);

          }
        });

        $scope.updateCounts();

      }, function errorCallback(response) {
        //TODO
      });

      /*
      * Uptdates the counts for each language. e.g. German (42) -> German (12)
      */
      $scope.updateCounts = function(){
        angular.forEach($scope.options, function(option){
          var optionHasCount = false;
          angular.forEach($scope.filterCounts, function(countedFilter){
            if(option.label === countedFilter.label){
              option.count = countedFilter.count;
              optionHasCount = true;
            }
          })
          if(optionHasCount === false){
            option.count = 0;
          }
        });
      }


      //Update the gui on changing filter Counts
      $scope.$watch('filterCounts', function(newVal, oldVal){
        $scope.updateCounts();
      });

      //add selected to query
      $scope.$watch('o.selected', function(newVal,oldVal){

        //remove all old language filters from queryObject
        angular.forEach(oldVal, function(selected){
          if($scope.queryObject['lang_'+ selected.label]){
            $scope.queryObject['lang_'+ selected.label] = false;
          }
        })

        //add all new language filters to queryObject
        angular.forEach(newVal, function(selected){
          $scope.queryObject['lang_'+ selected.label] = true;
        })

      }, true)


      //add selected to query
      $scope.$watch('queryObject', function(newVal,oldVal){

        angular.forEach($scope.options, function(option){
          //if should be selected, because it is in the queryObject
          if($scope.queryObject['lang_'+ option.label]){

            //if not yet selected
            var isSelected = false;
            for (var i = 0; i < $scope.o.selected.length; i++) {
              if($scope.o.selected[i].label === option.label){
                isSelected = true;
              }
            }

            //select it
            if(!isSelected){
              $scope.o.selected.push(option);
            }
          }
          //if should not be selected, because it is not in the queryObject
          else{
            var toDeselect = [];
            for (var i = 0; i < $scope.o.selected.length; i++) {
              //if selected
              if($scope.o.selected[i].label === option.label){
                //store index of elements to deselect
                toDeselect.push(i);
              }
            }
            //deselect
            for (var i = toDeselect.length -1; i >= 0; i--)
            $scope.o.selected.splice(toDeselect[i],1);
          }
        });

      }, true)


      //TODO UPdate o.selected when language filter removed from queryObject

      $scope.sorterFunc = function(option){
        return parseInt(option.count);
      };

      $scope.greaterThan = function(prop, val){
        return function(item){
          return item[prop] > val;
        }
      }

      $scope.isNotTrue = function(prop){
        return function(item){
          return item[prop] !== true;
        }
      }

    }],
    link: function(scope, element, attrs) {

    },
    templateUrl: 'languageFilter.html'
  };
})
.directive('genderFilter', function() {
  return {
    restrict: 'E',
    scope: {
      filterCounts: '=',
      queryObject: '='
    },
    controller: ['$scope', function($scope){
      //Update the gui on changing filter Counts
      $scope.$watch('filterCounts', function(newVal, oldVal) {

        //reset
        $scope.countF = 0;
        $scope.countM = 0;
        if(newVal){
          for (var i = 0; i < newVal.length; i++) {
            var row = newVal[i];

            if(row.label === 'feminin'){
              $scope.countF = row.count;
            }

            if(row.label === 'maskulin'){
              $scope.countM = row.count;
            }
          }
        }
      });

    }],
    templateUrl: 'genderFilter.html'
  };
})
.directive('timeFilter', function() {
  return {
    restrict: 'E',
    scope: {
      queryObject: '='
    },
    controller: ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http){

      //Defaults for slider floor and ceil (=extremes of biggest slider range)
      var defaultFloor = -200; //gets updated by http api call
      var defaultCeil = new Date().getFullYear();

      //Object with options for the time slider
      $scope.slider = {
        minValue: $scope.queryObject.startYear || defaultFloor,
        maxValue: $scope.queryObject.endYear || defaultCeil,
        options: {
          floor: defaultFloor,
          ceil: defaultCeil,
          step: 1,
          noSwitching: true,
          hidePointerLabels: true,
          hideLimitLabels: true,
          onChange: function(sliderId, modelValue, highValue, pointerType){
            $scope.inputMinValue = modelValue;
            $scope.inputMaxValue = highValue;
          }
        }
      };

      /*
      * fire http request to the earliestAndLatestYear api
      * get min and max year for appellation useage
      */
      $http({
        method: 'POST',
        url: 'api/earliest-and-latest-year'
      }).then(function successCallback(response) {

        //retrieve results
        try {
          //overwrite default floor
          $scope.slider.options.floor = response.data[0].min;

          //overwrite minValue if not specified by queryObject
          $scope.slider.minValue = $scope.queryObject.startYear || response.data[0].min;


        } catch (e) {
          //TODO
        } finally {

        }
      }, function errorCallback(response) {
        //TODO
      });

      //Update the query parameter on slide-end
      $scope.$on("slideEnded", function() {
        $scope.queryObject.startYear = $scope.slider.minValue;
        $scope.queryObject.endYear = $scope.slider.maxValue;
        $rootScope.$digest();
      });

      // //init inputs
      // $scope.$watch("queryObject.startYear", function() {
      //   $scope.inputMinValue = parseInt($scope.queryObject.startYear);
      // });
      // //init inputs
      // $scope.$watch("queryObject.endYear", function() {
      //   $scope.inputMaxValue = parseInt($scope.queryObject.endYear);
      // });

      $scope.inputMinValue =  parseInt($scope.queryObject.startYear) || defaultFloor;
      $scope.inputMaxValue =  parseInt($scope.queryObject.endYear) || defaultCeil;

      //set max
      $scope.setMax = function(){

        $scope.inputMaxValue = $scope.inputMaxValue ? $scope.inputMaxValue : 0;

        //do not allow max to be lower than minValue
        if($scope.inputMaxValue < $scope.slider.minValue){
          $scope.inputMaxValue = $scope.slider.minValue;
        }

        //do not allow max to be higher than ceil
        if($scope.inputMaxValue > $scope.slider.options.ceil){
          $scope.inputMaxValue = $scope.slider.options.ceil;
        }

        $scope.queryObject.endYear = $scope.slider.maxValue = $scope.inputMaxValue;
        $scope.queryObject.startYear = $scope.slider.minValue;

        $scope.updateBtn = false;
      }


      //set min
      $scope.setMin = function(){

        $scope.inputMinValue = $scope.inputMinValue ? $scope.inputMinValue : 0;

        //do not allow min to be higher than maxValue
        if($scope.inputMinValue > $scope.slider.maxValue){
          $scope.inputMinValue = $scope.slider.maxValue;
        }

        //do not allow min to be lower than floor
        if($scope.inputMinValue < $scope.slider.options.floor){
          $scope.inputMinValue = $scope.slider.options.floor;
        }

        $scope.queryObject.startYear = $scope.slider.minValue = $scope.inputMinValue;
        $scope.queryObject.endYear = $scope.slider.maxValue;

        $scope.updateBtn = false;
      }


      $scope.$watch('queryObject', function(newVal){

        if(!newVal.startYear){
          $scope.slider.minValue = $scope.inputMinValue = $scope.slider.options.floor;
        }

        if(!newVal.endYear){
          $scope.slider.maxValue = $scope.inputMaxValue = $scope.slider.options.ceil;
        }


      }, true)


      //refresh timeslider rendering
      $scope.refreshSlider = function () {
        $timeout(function () {
          $scope.$broadcast('rzSliderForceRender');
        });
      };

    }],
    templateUrl: 'timeFilter.html'
  };
})
.directive('resetFilterButton', function() {
  return {
    restrict: 'E',
    scope: {
      queryObject: '='
    },
    controller: ['$scope', function($scope){
      $scope.reset = function(){
        //reset gender filter
        $scope.queryObject.gender_feminin = false;
        $scope.queryObject.gender_maskulin = false;

        //reset time filter
        $scope.queryObject.startYear = false;
        $scope.queryObject.endYear = false;

        //reset language filter
        for (var i = 0; i < $scope.languages.length; i++) {
          $scope.queryObject[$scope.languages[i]] = false;
        }


      };


      $scope.languages = [];

      $scope.$watch('queryObject', function(newVal){
        $scope.hasFilter = false;

        angular.forEach(newVal, function(value, key){

          if((key === "gender_feminin" || key === "gender_maskulin") && value === true ){
            $scope.hasFilter = true;
          }

          if((key === "startYear" || key === "endYear")){
            $scope.hasFilter = true;
          }

          if(key.toString().indexOf('lang') === 0 && value === true ){
            $scope.hasFilter = true;

            $scope.languages.push(key);
          }

        })
      }, true)


    }],
    templateUrl: 'resetFilterButton.html'
  };
})
.directive('timeGraph', function() {
  return {
    restrict: 'E',
    scope: {
      timeline: '=',
      labelXAxis: '@',
      labelYAxis: '@'
    },
    controller: ['$scope', function($scope){
      var timeline = []

      for ( var i=0; i< $scope.timeline.length; i++)
      {
        if ($scope.timeline[i]['count'] != '0' )
        {
          var I = i;
          break;
          return I;
        }
      }

      for ( var i=I; i< $scope.timeline.length; i++)

      timeline.push($scope.timeline[i]);

      for (var i=timeline.length-1; i>=0 ; i--)

      if (timeline[i]['count'] && timeline[i]['count'] == '0' )
      {timeline.splice(i, 1);
        if (timeline[i-2]['count'] != '0')
        {break;}
      }

      //added "margins" inside the diagram, so that the left most column doesn't stick to the yAxis, and the right most is not too close to the edge
      var min = timeline[0]['start'] - 50 ;
      var min_obj = { "start" : min} ;
      timeline.unshift(min_obj);
      //adapt the wifth of the chart to the lenfth of the timeline array
      if (timeline.length < 12){
        var wid = timeline.length * 75;
      }
      else{
        var wid = 900;
      }
      $scope.options = {
        chart: {
          type: 'historicalBarChart',
          height: 350,
          width: wid,
          margin : {
            top: 20,
            right: 20,
            bottom: 65,
            left: 50
          },
          x: function(d){
            return d.start;
          },
          // y: function(d){return d[1]/100000;},
          y: function(d){
            cnt = parseInt(d.count, 10);
            return cnt;
          },
          showValues: true,
          // valueFormat: function(d){
          //     return d3.format(',.1f')(d);
          // },
          duration: 100,
          xAxis: {
            axisLabel: $scope.labelXAxis,
            // tickFormat: function(d) {
            // return d
            // },
            showMaxMin: false
          },
          yAxis: {
            axisLabel: $scope.labelYAxis,
            axisLabelDistance: -10,
            tickFormat: function(d){
              return d3.format("d")(d);
            }
          },
          tooltip: {
            keyFormatter: function(d) {
              // return d3.time.format('%x')(new Date(d));
              return d + ' - ' + (d + 49);
            }
          },
          zoom: {
            enabled: false,
            scale: 1,
            scaleExtent: [1, 10],
            useFixedDomain: false,
            useNiceScale: false,
            horizontalOff: false,
            verticalOff: true,
            unzoomEventType: 'dblclick.zoom'
          }
        }
      };

      $scope.data = [
        {
          "key" : "Quantity" ,
          "bar": true,
          "values" : timeline
        }];


      }],
      template: '<nvd3 options="options" data="data"></nvd3>'
    };
  })
  .directive('timeGraphSmall', function() {
    return {
      restrict: 'E',
      scope: {
        timeline: '=',
        labelXAxis: '@',
        labelYAxis: '@'
      },
      controller: ['$scope', function($scope){
        var timeline = []

        for ( var i=0; i< $scope.timeline.length; i++)
        {
          if ($scope.timeline[i]['count'] != '0' )
          {
            var I = i;
            break;
            return I;
          }
        }

        for ( var i=I; i< $scope.timeline.length; i++)

        timeline.push($scope.timeline[i]);

        for (var i=timeline.length-1; i>=0 ; i--)

        if (timeline[i]['count'] && timeline[i]['count'] == '0' )
        {timeline.splice(i, 1);
          if (timeline[i-2]['count'] != '0')
          {break;}
        }

        //added "margins" inside the diagram, so that the left most column doesn't stick to the yAxis, and the right most is not too close to the edge
        var min = timeline[0]['start'] - 50 ;
        var min_obj = { "start" : min} ;
        timeline.unshift(min_obj);

        $scope.options = {
          chart: {
            type: 'historicalBarChart',
            height: 350,
            // width: wid,
            margin : {
              top: 20,
              right: 20,
              bottom: 65,
              left: 50
            },
            x: function(d){
              return d.start;
            },
            // y: function(d){return d[1]/100000;},
            y: function(d){
              cnt = parseInt(d.count, 10);
              return cnt;
            },
            showValues: true,
            // valueFormat: function(d){
            //     return d3.format(',.1f')(d);
            // },
            duration: 100,
            xAxis: {
              axisLabel: $scope.labelXAxis,
              // tickFormat: function(d) {
              // return d
              // },
              showMaxMin: false
            },
            yAxis: {
              axisLabel: $scope.labelYAxis,
              axisLabelDistance: -10,
              tickFormat: function(d){
                return d3.format("d")(d);
              }
            },
            tooltip: {
              keyFormatter: function(d) {
                // return d3.time.format('%x')(new Date(d));
                return d + ' - ' + (d + 49);
              }
            },
            zoom: {
              enabled: false,
              scale: 1,
              scaleExtent: [1, 10],
              useFixedDomain: false,
              useNiceScale: false,
              horizontalOff: false,
              verticalOff: true,
              unzoomEventType: 'dblclick.zoom'
            }
          }
        };

        $scope.data = [
          {
            "key" : "Quantity" ,
            "bar": true,
            "values" : timeline
          }];


        }],
        template: '<nvd3 options="options" data="data"></nvd3>'
      };
    })
    .directive('metagridLinks', function() {
      return {
        restrict: 'E',
        scope : {
          hlsId : '@',
          ssrqId : '@'
        },
        controller: ['$scope', '$http', function($scope, $http){

          $scope.data = {};

          $scope.loading = 0;

          $scope.onceVisible = false;
          $scope.inView = function(inview, inviewInfo) {
            //when first time visible
            if(!$scope.onceVisible && inview && inviewInfo.event.type !== 'initial'){
              $scope.onceVisible = true;

              /*
              * http request to metagrid for hls_id
              */
              if($scope.hlsId !== 'null'){

                $scope.loading ++;

                $http.jsonp('https://api.metagrid.ch/widget/hls-dhs-dss/person/'+$scope.hlsId+'.json?lang=de&include=true&jsoncallback=JSON_CALLBACK')
                .success(function(response) {

                  $scope.hls_metagrid_links =   response[0];


                  $scope.loading --;
                }).error(function (error) {

                  $scope.loading --;

                  //TODO
                });
              }

              /*
              * http request to metagrid for ssrq_id
              */
              if($scope.ssrqId !== 'null'){

                $scope.loading ++;

                $http.jsonp('https://api.metagrid.ch/widget/ssrq/person/'+$scope.ssrqId+'.json?lang=de&include=true&jsoncallback=JSON_CALLBACK')
                .success(function(response) {

                  $scope.ssrq_metagrid_links =   response[0];

                  $scope.loading --;
                }).error(function (error) {

                  $scope.loading --;

                  //TODO
                });
              }

            }
          }


        }],
        templateUrl: 'metagridLinks.html'
      }
    });
