angular.module('App')
.controller('SearchController', ['$scope', '$location', '$http', '$timeout', '$httpParamSerializer',
function($scope, $location, $http, $timeout, $httpParamSerializer) {

  /*************** Logic for Search Page ***********/

  //function to switch from advanced to simple search
  $scope.showSimpleSearch = function (){

    //Object to store defaults of simple search
    var simpleSearchDefaults = {
      'advancedSearch' : false,
      'searchIn' : 'all',
      'matchType' : 'contains'
    };

    //reset query parameters to simple search defaults
    angular.merge($scope.query, simpleSearchDefaults)
  }



  /*** Watch Query Parameters and fire Api Calls ***/

  var timeoutPromise;


  /*
  / Fire Api Call and Query for ForenamesTest
  / delayInMs: Timeout for calling the query in milliseconds (default 0)
  */
  var runQuery = function (delayInMs){

    //set default for delay
    delayInMs = delayInMs ? delayInMs : 0;

    //does nothing, if timeout alrdy done
    $timeout.cancel(timeoutPromise);

    //Set timeout
    timeoutPromise = $timeout(function(){


      // //if some searchString is there do the request
      // if($scope.query.searchString && $scope.query.searchString.length > 0) {



        //set loading flag to true (used for displaying a spinner)
        $scope.loading = true;
        $scope.languageCountsLoading = true;


        console.log('Api Call fired')

        //create the data object
        var data = {};
        // angular.merge(data, $scope.query,$scope.pagination)

        //fire http request to the forenames search api
        $http({
          method: 'POST',
          url: 'api/forenames',
          data: $scope.query
        }).then(function successCallback(response) {

          //retrieve results
          $scope.results = response.data;

          //reset spinner
          $scope.loading = false;

          //flag says that api was successfully called at least one time
          //(used for toggle visibility of gui element)
          $scope.apiCalled = true;

        }, function errorCallback(response) {

          //TODO

        });

        //fire http request to the languageCounts search api
        $http({
          method: 'POST',
          url: 'api/language-counts',
          data: $scope.query
        }).then(function successCallback(response) {

          //retrieve results
          $scope.languageCounts = response.data;

          //reset spinner
          $scope.languageCountsLoading = false;

          //flag says that api was successfully called at least one time
          //(used for toggle visibility of gui element)
          $scope.languageCountsApiCalled = true;

        }, function errorCallback(response) {

          //TODO

        });


        //fire http request to the gender-counts search api
        $http({
          method: 'POST',
          url: 'api/gender-counts',
          data: $scope.query
        }).then(function successCallback(response) {

          //retrieve results
          $scope.genderCounts = response.data;

        }, function errorCallback(response) {

          //TODO

        });
      // }

    }, delayInMs);
  }

  /*
  * Clears the query results
  * onlyRows: boolean, if true only results.rows will be reset, count stays
  */
  var clearResults = function(onlyRows){
    if(onlyRows){

      //reset only results.rows (count needs to stay for pagination)
      if($scope.results){
        $scope.results.rows = {};
      }

    }else{

      //reset all results including results.count
      $scope.results = {};

    }
  }


  //Watch the query object an fire api call for query
  $scope.$watch('query', function(newQueryParams, oldQueryParams) {

    //prevent searching on a page load when old an new are still equal
    if(newQueryParams !== oldQueryParams){

      //set delay in milliseconds
      var delayInMs = 1000;

      //flag to say if the total number of results will change
      var onlyRows = false;

      //if the change is only the current page in pagination or order-option
      if(newQueryParams.currentPage !== oldQueryParams.currentPage || newQueryParams.order !== oldQueryParams.order){
        delayInMs = 0;
        onlyRows = true;
      }

      clearResults(onlyRows);

      runQuery(delayInMs);

      //deep copy of params to prevent that they get changed
      var params = {};
      var params = angular.copy(newQueryParams);

      //Update Url Parameters
      angular.forEach(params, function(val, paramName){
        val = val === false ? null : val;

        $location.search(paramName, val);
      })


    }
  }, true);

  //fire api call for query without delay
  $scope.queryNow = function(){

    //set delay in milliseconds
    var delayInMs = 0;
    runQuery(delayInMs);

  }

  initializeQuery();

  // Initialize the query object according to the url parameters
  function initializeQuery(){
    var query = $location.search();

    //set current page to 1 by default
    query.currentPage = query.currentPage ? query.currentPage : 1;

    $scope.query = query;

    //run query if the url provided some parameters
    Object.keys($scope.query).length > 0 ? runQuery() : '';
  }


  //count elements in object
  $scope.count = function(obj){
    return  _.keys(obj).length;
  }

}]).controller('engageOne', ['$scope', '$http', function($scope, $http) {

  $scope.send = function(){

    //form validation
    if($scope.emailForm.input.$valid){
      $scope.formInvalid = false;
      $scope.loading = true;

      //fire http request
      $http({
        method: 'POST',
        url: 'api/register-new-mail',
        data: {
          email:
          $scope.email.text
        }
      }).then(function successCallback(response) {
        $scope.registeredEmailAddress = response.data.registeredEmailAddress;
        $scope.emailConfirmatoin = true;
        $scope.loading = false;

      }, function errorCallback(response) {

        //TODO

      });
    }else{
      $scope.formInvalid = true;

    }
  }

}]).controller('engageTwo', ['$scope', '$http', function($scope, $http) {

  $scope.send = function(){

    //form validation
    if($scope.feedbackForm.$valid){
      $scope.formInvalid = false;
      $scope.loading = true;

      //fire http request
      $http({
        method: 'POST',
        url: 'api/send-feedback',
        data: $scope.feedback
      }).then(function successCallback(response) {

        $scope.confirmation = true;
        $scope.loading = false;

      }, function errorCallback(response) {

        //TODO

      });
    }else{
      $scope.formInvalid = true;
    }
  }
}]).controller('forenameController', ['$scope', function($scope) {

}])
