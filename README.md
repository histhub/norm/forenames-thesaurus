# histHub forenames

## The histHub Forenames Thesaurus (https://thesaurus.histhub.ch/forenames/)

To get it up and running
* install Node.js v10.x
* install npm unless contained in nodejs package
* install node modules needed with 'npm install' (ignore all warnings ;-)
* edit DB connection (`postgres_dev`) in `config/connections.js`
* set environment variables needed for Sails:
  * export sails_connections__postgres_dev__password=MyVerySecretPassword
  * export sails_grunt___hookTimeout=60000
* start server (in development environment) with 'node app.js'
